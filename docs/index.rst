.. PARyOpt documentation master file, created by
   sphinx-quickstart on Thu May  3 17:31:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PARyOpt's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   introduction
   source/PARyOpt
   source/modules
   examples/index

PARyOpt (pronounced **pur-yopt**, with a hard *y*) is a modular asynchronous Bayesian optimization package that enables
remote function evaluation.

It is written to solve optimization problems where the cost function is very expensive to evaluate
(on the order of hours), and where cost functions must be evaluated on remote machines (HPC clusters).

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
